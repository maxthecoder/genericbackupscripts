#!/bin/bash
########################################################################################################################
# Author:   Daniel Ciulinaru
# Email:    daniel.ciulinaru@global.ntt
# Purpose:  extract the DB_NAME parameter and pass it onto the calling script
#           The db_name value is used to create the folder under which the backups pieces are stored
#           Reliance /{backup_home}/{shorthostname}/databases/{db_name} allows the connectivity to the database to
#           happen via any sid/db_name/service_name/tns alias

# variables:
script_name="$(basename $0)";
readlink="$(which readlink 2>&1)" || \
if [ $? -ne 0 ]; then
	printf "%s\n" "$script_name:      unable to correctly determine location of readlink";
	printf "%s\n" "$script_name:      $readlink";
	exit 1;
fi;
scripts_home="$(dirname $($readlink -f $0) 2>&1)" || \
if [ $? -ne 0 ]; then
	printf "%s\n" "$script_name:      unable to correctly determine the scripts absolute path";
	printf "%s\n" "$script_name:      $scripts_home";
	exit 1;
fi;

# get the life cycle status keyword:
if [ -z "$1" ]; then
	printf "%s\n" "$script_name:      the first argument cannot be empty. It needs to be a valid group name: dv, qa, dr, pd";
	exit 1;
else
	env_tag=$(echo "$1" | tr '[:upper:]' '[:lower:]');
fi;

# get the db/sid,
if [ -z "$2" ]; then
	printf "%s\n" "$script_name:      the second argument cannot be empty. It needs to be the database name (not the SID), provided by the db_name parameter";
	exit 1;
else
	sid=${2///};
	# Check if the incoming argument exists in /etc/oratab
	inoratab=$(cat /etc/oratab | egrep -v "^#|ASM|MGMTDB|egrep" | egrep -v "^$" | cut -d ":" -f 1 | sort | uniq | grep -oE "(^|\s)$sid($|\s)" | wc -w)
	if [[ "$inoratab" -eq "0" ]]; then
		printf "%s\n" "$script_name:      $sid does not exist in /etc/oratab";
		exit 1;
	fi;
fi;

# Set the environment
export PATH=/usr/local/bin:$PATH
export ORACLE_SID=$sid
oraenv=$(command -v oraenv 2>/dev/null) || if [ $? -ne 0 ]; then echo -e "oraenv could not be found"; exit 1; fi;
export ORAENV_ASK=NO
. "$oraenv" 2>/dev/null 1>/dev/null;
oracle_home="$ORACLE_HOME";
export NLS_LANGUAGE="AMERICAN_AMERICA.UTF8";
export NLS_DATE_FORMAT="DD-MON-YYYY HH24:MI:SS";
sqlplus="${oracle_home}/bin/sqlplus";

# get the authentification info:
local_password_file="${scripts_home}/.${env_tag}.password";
. "$local_password_file" 2>/dev/null || \
if [ $? -ne 0 ]; then
	printf "%s\n" "$script_name:      unable to extract the $env_tag password.";
	printf "%s\n" "$script_name:      the $env_tag password is supposed to be stored at: ${local_password_file}. Exit";
	exit 1;
fi;
# Checking the variables: username, password; if "needscatalog" == yes, check the associated variables as well:
if [ -z "$username" ]; then
	printf "%s\n" "$script_name:      the 'username' variable has not been correctly declared in: ${local_password_file}";
	printf "%s\n" "$script_name:      it has to be in the format 'username=value', no quotes";
	printf "%s\n" "$script_name:      exiting";
	exit 1;
fi;
if [ -z "$password" ]; then
	printf "%s\n" "$script_name:      The 'password' variable has not been correctly declared in: ${local_password_file}";
	printf "%s\n" "$script_name:      It has to be in the format 'password=value', no quotes";
	printf "%s\n" "$script_name:      exiting";
	exit 1;
fi;


# define functions:
fn_db_name()
{
	# SQLPLUS wrapper, used by other functions, for expediency and consistency
	# makes a generic connection to the database
	# expects 2 local parameters:
	#	formatscript 	- to format the output
	#	sqlscript		- the actual sql command
	# returns the result to the calling function
	local formatscript="
set linesize 200 embedded on tab off pagesize 0 head off feed off echo off
set timing off
col SCN format a20"
	local sqlscript="select value from v\$system_parameter2 where name='db_name'";

	if [ "$username" == "SYS" ] || [ "$username" == "sys" ] ;
	then
		connection=$username'/"'$password'"@'$sid' as sysdba';
	else
		connection=$username'/"'$password'"@'$sid;
	fi;

	local result="$($sqlplus -S -L "$connection"<<EOF
$formatscript;
$sqlscript
/
EOF
)";
	local error_check;
	error_check="$(echo -e "${result}" | tr -d '[[:space:]]')"
	if [[ "$error_check" =~ .*"ORA-".* ]];
	then
		printf "%s\n" "$script_name:      unable to retrieve db_name parameter:";
		printf "%s\n" "$script_name:      connection used: $connection";
		printf "%s\n" "$script_name:      $result";
		exit 1;
	fi;
	printf "%s" "$result";
}
fn_db_name;