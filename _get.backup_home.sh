#!/usr/bin/env python
########################################################################################################################
# Author:   Daniel Ciulinaru
# Email:    daniel.ciulinaru@global.ntt
# Purpose:  one script that provides a hardcoded $backup_home location to all calling scripts.
# The $backup_home needs not to be determined dynamically.
import sys
import os
import socket

backup_mount="/backup"
shorthostname=socket.gethostbyaddr(socket.gethostname())[0].split(".")[0]

def main():
    script_name=os.path.basename(sys.argv[0])
    if not os.path.exists(backup_mount):
        print("{}:  {} mount point folder does not exist".format(script_name, backup_mount))
        sys.exit(1)
    if not os.path.isdir(backup_mount):
        print("{}:  {} is not a folder".format(script_name, backup_mount))
        sys.exit(1)
    if not os.path.ismount(backup_mount):
        print("{}:  {} path is not a mounted point".format(script_name, backup_mount))
        sys.exit(1)
    print(os.path.join(backup_mount, shorthostname))

if __name__== "__main__":
   main()