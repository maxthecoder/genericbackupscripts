#!/bin/bash
########################################################################################################################
# Author:   Daniel Ciulinaru
# Email:    daniel.ciulinaru@global.ntt
# Purpose:  generate a 10-characters database hash unique identifier composed from the hostname/rac name and
#           the db_name value. It is used to correctly identify the RAC databases when the backups are triggered
#           from different nodes. The dbid of the database is awfully unreliable because database refreshes can happen
#           and even having different database names with the same dbid. Hence, such a synthetic dbhash becomes necessary
script_name="$(basename $0)";
if [ -z "$1" ]; then
	printf "%s\n" "$script_name:      the first argument cannot be empty. It needs to be a short form hostname value";
	exit 1;
fi;
if [ -z "$2" ]; then
	printf "%s\n" "$script_name:      the second argument cannot be empty. It needs to be a db_name value";
	exit 1;
fi;

hostname=${1};
db_name=${2};
sha1="$(which sha1sum | tr -d [[:space:]])"
if [ $? -ne 0 ]; then
    printf "%s\n" "$script_name:      unable to correctly locate the sha1sum utility";
    exit 1;
fi;
sha1_sum="$(echo ${db_name}\|${hostname} | tr -d [[:space:]] | $sha1 | head -c 10 | tr -d [[:space:]])";
printf "%s" "$sha1_sum";