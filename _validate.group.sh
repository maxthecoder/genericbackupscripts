#!/usr/bin/env python
########################################################################################################################
# Author: Daniel Ciulinaru
# Email:  daniel.ciulinaru@secure-24.com
# Purpose: validate the group names: DV, PY, SB, QA, DR, PD

# validate the life cycle status keyword:
import sys
import os

script_name=os.path.basename(sys.argv[0])
accepted_list = ['dv', 'qa', 'pd', 'dr']
formatted_list = ", ".join(accepted_list)
try:
    group = sys.argv[1].lower()
    if group not in accepted_list:
        print("%s:      The environment '%s' two-letter keyword is not in the accepted list: %s" % (script_name, sys.argv[1], formatted_list))
        sys.exit(1)
    print(group.strip())
except IndexError:
    print("%s:  The environment two-letter keyword cannot be empty. Example of values: %s" % (script_name, formatted_list))
    sys.exit(1)