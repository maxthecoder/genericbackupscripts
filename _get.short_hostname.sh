#!/usr/bin/env python
########################################################################################################################
# Author:   Daniel Ciulinaru
# Email:    daniel.ciulinaru@global.ntt
# Purpose:  returns the short form hostname in lower case

import socket
print(socket.gethostbyaddr(socket.gethostname())[0].split(".")[0].lower())