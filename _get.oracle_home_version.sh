#!/usr/bin/env python
########################################################################################################################
# Author:   Daniel Ciulinaru
# Email:    daniel.ciulinaru@global.ntt
# Purpose:  determine the Oracle home version
import sys
import os
import re

def main():
    script_name=os.path.basename(sys.argv[0])
    try:
        oracle_home=sys.argv[1]
    except IndexError:
        print("%s:  Oracle home argument not passed" % script_name)
        sys.exit(1)
    comps_xml=os.path.join(oracle_home, "inventory", "ContentsXML", "comps.xml")
    try:
        pattern='<COMP NAME="oracle.server"'
        full_version_pattern=re.compile('^[0-9]+\.[0-9]\.[0-9]\.[0-9]\.[0-9]$')
        file=open(comps_xml, "r")
        for line in file:
            if re.search(pattern, line):
                full_version=line.split(" ")[2].split("=")[1].strip('"')
                if full_version_pattern.search(full_version):
                    print(full_version.split(".")[0])
    except IOError:
        print("%s:  %s file does not exist or is not readable" % (script_name, comps_xml))
        sys.exit(1)
    else:
        file.close()
        sys.exit(0)

if __name__== "__main__":
   main()